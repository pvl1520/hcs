import Schema from './schema';

import object_value from '/imports/api/lib/helpers/object_value';
import format_date from '/imports/api/lib/helpers/format_date';
import moment from 'moment-timezone';
import numeral from 'numeral';

const Results = new Mongo.Collection('Results');

Results.attachSchema(Schema);

Results.allow({
  insert() { return true; }
});

Results.deny({
  update() { return true; },
  remove() { return true; },
});

// Collection helpers (dburles:collection-helpers)
Results.helpers({
  collection_helper__time() {
    let
      start = object_value(this, 'start'),
      end = object_value(this, 'end'),
      now = new Date();

    if ( !(start && moment(start).isValid()) ) {
      return;
    }

    function what_full_format (date) {
      return moment(date).isSame(now, 'year')
        ? 'MM-DD HH:mm' //'MM-DD HH:mm z'
        : 'YYYY-MM-DD HH:mm' //'YYYY-MM-DD HH:mm z';
    }

    let get_start = `
        <span class="no-wrap">${ format_date({date: start, formatted: 'named'}) }</span><br>
        <small class="no-wrap">${ moment(start).tz('Europe/Berlin').format( what_full_format(start) ) }</small>`;

    let get_end = end
      ? ( moment(start).isSame(end, 'day')
          ? `&nbsp;— <small class="no-wrap">${ moment(end).tz('Europe/Berlin').format('HH:mm') }</small>` //.format('HH:mm z')
          : `&nbsp;— <small class="no-wrap">${ moment(end).tz('Europe/Berlin').format( what_full_format(end) ) }</small>`
      )
      : '';

    return get_start + get_end;
  },
  collection_helper__duration() {
    let
      start = object_value(this, 'start'),
      end = object_value(this, 'end');

    if (!start || !end) {
      return;
    }

    start = moment(start);
    end = moment(end);

    let duration = end.diff(start, 'seconds');

    return numeral(duration).format('00:00:00');
  }
});

export default Results;