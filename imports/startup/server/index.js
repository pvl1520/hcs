﻿// Set process environments
import './process_env_MAIL';

import '/imports/startup/both';

import './ssr-head';
import './publications';
import './methods';

// import Results from '/imports/api/results/collection';

// Results.insert({
//   "players": ["player 1", "player 2"],
//   "winner": "player 1",
//   "date": new Date,
//   "duration": "10",
//   "continent": "Asia"
// });