import Collection from '../collection';

Meteor.publish('pub__results', function (search = {}) {

  let options = {
    // fields: {
    //   _id: 1,
    //   name: 1
    // }
  };

  return Collection.find(search, options);
});