import object_value from '/imports/api/lib/helpers/object_value';

/**
 * Get image url
 * @param {string} _id
 * @param {string} [version]
 * @returns {*|Boolean} Returns value or false.
 *
 * TODO: @mrauhu, can we Use [`.link()`](https://github.com/VeliovGroup/Meteor-Files#stream-files)?
 */
export default function  image_url (_id, version = 'original') {
  if (!_id) {return false}

  let image = Images.findOne(_id);
  if (!image) {return false}

  return object_value(image, 'versions.' + version + '.meta.pipeFrom')
    || object_value(image, 'versions.original.meta.pipeFrom')
    || object_value(image, 'versions.original.path');
};