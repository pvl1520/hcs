// Array for Blaze.View's
let views = [];
let count_views = 0;

// TODO: Rewrite as class
Modal = {
  /**
   * Open modal window
   * @param {Object} options
   * @param {Object|String} options.data
   * @param {String} [options.modal_template]
   * @param {Object} [options.data.html_attributes]
   * @param {Array} [options.data.subscribe]
   * @param {String|Array} [options.data.subscribe[n]]
   * @param {String} [options.data.subscribe[n][0]] Publication name
   * @param {*} [options.data.subscribe[n][1]] Publication arguments
   * @param {Object} [options.parent_node] TODO
   */
  open (options) {
    const modal_template = typeof options.modal_template !== "undefined"
      ? options.modal_template
      : 'modal';

    const $body = $("body");
    $body.attr("data-modal-opened", true);

    // Compensate body scroller
    if ( $(document).height() > $(window).height() ) {
      $body.addClass("no-scroll");
    }

    // Parent node
    const parent_node = typeof options.parent_node !== "undefined"
      ? options.parent_node
      : document.body;

    // Modal data
    const data = options.data;

    // Modal HTML attributes
    data['html_attributes'] = typeof data['html_attributes'] !== "undefined"
      ? data['html_attributes']
      : {};

    data['html_attributes']['data-modal-vertical'] = typeof data['html_attributes']['data-modal-vertical'] !== "undefined"
      ? data['html_attributes']['data-modal-vertical']
      : "top";

    // Render view
    const view = Blaze.renderWithData(Template[modal_template], data, parent_node);

    // Add rendered view to all views array
    views.push(view);

    console.log("Modal.open #1 - count_views:", count_views);

    // Count view
    if (count_views > 0) {
      ++count_views;
    } else {
      count_views = 1;
    }

    // Reset AutoForms
    AutoForm.resetForm("modal");

    console.log("Modal.open #2 - count_views:", count_views);
  },

  /**
   * Close modal window
   * @param [view] (`instanceof Blaze.View`) If no `view` passed, all modals views will be removed.
   */
  close (view) {
    console.log("Modal.close #1 - count_views:", count_views);

    const remove_view = function(view) {
      console.log("Modal.close #2a - count_views:", count_views);
      if (view instanceof Blaze.View) {
        let r = Blaze.remove(view);
        // Count view
        if (count_views > 0) {
          --count_views;
        }
        console.log("Modal.close #2b - count_views", count_views);
      }
    };

    if (typeof view !== "undefined") {
      console.log("Modal.close #3 - count_views", count_views);
      remove_view(view);
    } else {
      // Remove all views
      _.each(views, function(view){
        console.log("Modal.close #4 - count_views", count_views);
        remove_view(view);
      });
    }

    // TODO: Do not remove `body` attributes if other modals still exists.
    if (count_views < 1) {
      $("body")
        .removeAttr("data-modal-opened")
        .removeClass("no-scroll");
    }

    console.log("Modal.close #5 - count_views:", count_views);
  }
};

Template.modal.onCreated(function () {
  const self = this;

  // Subscribe
  if (typeof self.data.subscribe !== "undefined" && _.isArray(self.data.subscribe)) {
    self.autorun(function () {
      _.each(self.data.subscribe, function (subscribe) {
        console.log("Template.modal.onCreated - subscribe_obj:", subscribe);

        if (_.isString(subscribe)) {

          self.subscribe(subscribe);

        } else if (_.isArray(subscribe)) {

          if (typeof subscribe[1] !== "undefined") {
            self.subscribe(subscribe[0], subscribe[1]);
          } else {
            self.subscribe(subscribe[0]);
          }

        }
      });
    });
  }
});

Template.modal.onRendered(function(){
  const self = this;

  self.$('[autofocus]').focus();
});

Template.modal.events({
  "click .js-modal-close": function (event, template) {
    Modal.close(template.view);
  }
});

Template.body.events({
  "click .js-all-modals-close": function () {
    Modal.close();
  },
  "click [data-modal-template]": function (event) {
    const $this = $(event.currentTarget),
      template = $this.data('modal-template');

    let html_attributes = {
      'data-modal-content-no-padding': true
    };

    Modal.open({
      data: {
        template: template,
        html_attributes
      }
    });
    event.preventDefault();
  },
  "click [data-modal-text]": function (event) {
    const $this = $(event.currentTarget),
      content = $this.data('modal-text');
    Modal.open({
      data: {
        template: 'modal_blank',
        content: content
      }
    });
    event.preventDefault();
  }
});

AutoForm.hooks({
  modal: {
    onError: function () {
      console.log("AutoForm.hooks #modal - onError - this:", this);
    },
    onSuccess: function(update, result){
      if (result) {
        // console.log("AutoForm.hooks #modal - onSuccess - this:", this);
        // console.log("AutoForm.hooks #modal - onSuccess - update:", update);
        // console.log("AutoForm.hooks #modal - onSuccess - result:", result);

        // TODO: Close only current modal view.
        Modal.close();
      }
    }
  }
});