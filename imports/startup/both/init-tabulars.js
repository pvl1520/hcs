import Tabular from 'meteor/aldeed:tabular';
import * as Tabulars from './tabulars';

Meteor.startup(()=> {

  /**
   * Tabular schema definition
   * @type {*}
   */
  let tabular_schema = {};

  let keys = Object.keys(Tabulars); // because Obj in `import * as Obj from ...` doesn't have `.length` or `.prototype`

  for (let k = 0, kk = keys.length; k < kk; k++) {
    tabular_schema = Tabulars[keys[k]];

    //console.log('Tabular schema', tabular_schema);

    let _defaults = Object.assign({}, tabular_defaults);
    let merged = Object.assign(_defaults, tabular_schema);

    new Tabular.Table(merged);
    console.log('Init tabular: ' + tabular_schema.name);
  }

});

/**
 * Tabular defaults
 * @type {*}
 */
let tabular_defaults = {
  sub: new SubsManager(), // use Subscription manager, gently

  search: {
    caseInsensitive: true,
    smart: true,
    regex: false,
    onEnterOnly: false,
  },
  // Pagination
  paging: true,
  lengthChange: false,
  pageLength: 30,
  limit: 30,
};

