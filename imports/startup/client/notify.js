/**
 * Notify.js
 * @type {*}
 */

const notify = require('notifyjs-browser');
notify($);

/**
 * Config options
 * @see https://notifyjs.com
 */
$.notify.defaults({
  // whether to auto-hide the notification
  autoHide: true,
  autoHideDelay: 5000,
  globalPosition: 'bottom right',
  // show animation
  showAnimation: 'fadeIn',
  // show animation duration
  showDuration: 400,
  // hide animation
  hideAnimation: 'fadeOut',
  // hide animation duration
  hideDuration: 200,
});

window.$notify_error = function (message) { $.notify(message, 'error')};
window.$notify_warning = function (message) { $.notify(message, 'warning')};
window.$notify_success = function (message) { $.notify(message, 'success')};

window.$notify = $.notify;