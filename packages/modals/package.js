Package.describe({
  name: 'anonima:modals',
  version: '0.4.0',
  summary: 'Modals with dynamic template. Inspired by peppelg:bootstrap-3-modal.'
});

Package.onUse(function(api) {
  api.versionsFrom('1.1.0.3');
  api.export("Modal");
  api.use([
    'jquery',
    'templating',
    'session',
    'ui',
    'aldeed:autoform',
    'gwendall:body-events'
  ]);
  api.addFiles('lib/client/modals.html', 'client');
  api.addFiles('lib/client/modals.js', 'client');
});
