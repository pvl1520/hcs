import moment from 'moment';
// Loading locales
//import '/node_modules/moment/locale/x-pseudo'; // for debug
import '/node_modules/moment/locale/en-gb';
import '/node_modules/moment/locale/ru';


/**
 * Get formatted date
 * @param {string} date
 * @param {string} [locale] If not set using `TAPi18n.getLanguage()`
 * @param {string} [formatted]
 * @returns {*|Boolean} Returns value or false.
 */
export default function format_date ({date, locale = TAPi18n.getLanguage(), formatted = 'named, numbered'}) {
  if (!date || !(date instanceof Date)) {
    return;
  }
  //console.log('format_date() - locale before', locale);

  let moment_date = moment(date).locale(locale);

  const formatted_date_exact = moment_date.format('YYYY-MM-DD HH:mm');

  let named = (moment().diff(date, 'days') >= 1)
    ? moment_date.fromNow() // '2 days ago' etc.
    : moment_date.calendar().split(' ')[0]; // 'Today', 'yesterday', 'tomorrow'

  let result;

  switch (formatted) {
    case 'named, numbered':
      result = named + ', ' + formatted_date_exact;
      break;
    case 'named':
      result = named;
      break;
    case 'numbered':
      result = formatted_date_exact;
      break;
    case 'two lines':
      result =
        '<span class="no-wrap">'+named+'</span><br>' +
        '<small class="no-wrap">'+formatted_date_exact+'</small>';
      break;
    default:
      result = formatted_date_exact;
      break;
  }

  return result;
};