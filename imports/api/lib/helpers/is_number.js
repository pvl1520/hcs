// https://stackoverflow.com/a/9716515

/**
 * Check if value is number
 * @param n
 * @returns {boolean}
 */
export default function is_number(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}