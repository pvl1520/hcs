/**
 * Results
 */
import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions({'autoform': Match.Optional(Match.Any)});

import moment from 'moment';
import '/node_modules/moment/locale/en-gb';

let players = [
  {label: "Campbell Brian", value: "Campbell Brian"},
  {label: "Pierce Daniel", value: "Pierce Daniel"},
  {label: "Wilson Peter", value: "Wilson Peter"},
  {label: "Brandt-Richards Dale", value: "Brandt-Richards Dale"},
  {label: "Connelly Austin", value: "Connelly Austin"},
  {label: "Sarah T. Martin", value: "Sarah T. Martin"},
  {label: "Juanita G. Jones", value: "Juanita G. Jones"},
  {label: "Tormented Agafonova", value: "Tormented Agafonova"},
  {label: "Iskander Alexeieva", value: "Iskander Alexeieva"},
  {label: "Arkhip Mishin", value: "Arkhip Mishin"},
  {label: "Philip Vilhelmsen", value: "Philip Vilhelmsen"},
  {label: "Oliver Prestegård", value: "Oliver Prestegård"},
  {label: "Elise Høie", value: "Elise Høie"},
  {label: "Kyoko Kurota", value: "Kyoko Kurota"}
];

export default new SimpleSchema({
  players: {
    type: Array,
    optional: false,
    autoform: {
      type: 'universe-select',
      afFieldInput: {
        options: players,
        multiple: true,
        create: true,
        uniPlaceholder: 'Select player',
        optionsPlaceholder: 'Unselect all'
      }
    }
  },
  'players.$': String,
  winner: {
    type: String,
    optional: false,
    autoform: {
      type: 'universe-select',
      afFieldInput: {
        options: players,
        uniPlaceholder: 'Select winner'
      }
    }
  },
  start: {
    type: Date,
    label: 'Start (CEST)',
    optional: false,
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        timezoneId: "Europe/Berlin",
        dateTimePickerOptions: {
          locale: moment.locale('en'),
          format: "YYYY-MM-DD HH:mm",
          sideBySide: true,
          showTodayButton: true,
          showClear: true
        }
      }
    }
  },
  end: {
    type: Date,
    label: 'End (CEST)',
    optional: false,
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        timezoneId: "Europe/Berlin",
        dateTimePickerOptions: {
          locale: moment.locale('en'),
          format: "YYYY-MM-DD HH:mm",
          sideBySide: true,
          showTodayButton: true,
          showClear: true
        }
      }
    },
    custom() {
      if (typeof this.field('start').value !== "undefined") {
        let start = this.field('start').value;

        if (this.value < start) {
          return "min_end_date";
        }
      }
    }
  },
  continent: {
    type: String,
    optional: false,
    autoform: {
      type: 'universe-select',
      afFieldInput: {
        options: [
          {label: "Asia", value: "Asia"},
          {label: "Africa", value: "Africa"},
          {label: "Antarctica :)", value: "Antarctica"},
          {label: "Australia", value: "Australia"},
          {label: "Europe", value: "Europe"},
          {label: "North America", value: "North America"},
          {label: "South America", value: "South America"}
        ],
        multiple: false,
        uniPlaceholder: 'Select continent'
      }
    }
  },
  createdAt: {
    type: Date,
    optional: true,
    denyUpdate: true,
    autoform: {
      type: 'hidden'
    },
    autoValue: function () {
      if (this.isInsert) {
        return new Date;
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date};
      } else {
        this.unset();
      }
    }
  },
  updatedAt: {
    type: Date,
    optional: true,
    autoform: {
      type: 'hidden'
    },
    autoValue: function () {
      return new Date;
    }
  }
}, { tracker: Tracker });