export let numeral = require('numeral');

// load a language
numeral.register('locale', 'ru', {
  delimiters: {
    thousands: ' ',
    decimal: ','
  },
  abbreviations: {
    thousand: 'тыс.',
    million: 'млн',
    billion: 'млрд',
    trillion: 'трлн'
  },
  ordinal : function (number) {
    return number === 1 ? 'er' : 'ème';
  },
  currency: {
    symbol: 'руб.'
  }
});

// switch between languages
// numeral.locale('ru');

numeral.defaultFormat('0,0[.]00');