import Collection from './collection';

import moment from 'moment-timezone';

//noinspection JSUnusedGlobalSymbols
export default {
  name: Collection._name,
  collection: Collection,
  extraFields: ['start', 'end'],
  responsive: true,
  autoWidth: false,
  stateSave: false,
  pageLength: 10,
  limit: 10,
  searching: false,
  // skipCount: true,
  // pagingType: 'numbers',
  columnDefs: [
    // { targets: '_all', orderable: false },
  ],
  order: [[ 6, 'desc' ]], // Starts from 0
  columns: [
    {
      titleFn() {return TAPi18n.__('Players')},
      width: "15%",
      data: 'players',
      orderable: false,
      render: function (value, type, doc) {
        if (Array.isArray(value)) {
          let string = '';

          value.map(name => {
            string = string + '<span class="f-results-list-item">' + name + '</span>'
          });

          return string;
        }
      }
    },
    {
      titleFn() {return TAPi18n.__('Winner')},
      width: "15%",
      data: 'winner'
    },
    {
      titleFn() {return TAPi18n.__('Time (CEST)')},
      width: "15%",
      data: 'collection_helper__time()',
      orderable: true,
      orderData: [6]
    },
    {
      titleFn() {return TAPi18n.__('Duration')},
      width: "1%",
      className: "f-table-numbers",
      orderable: true,
      tmpl: Meteor.isClient && Template.frontend__results__duration
    },
    {
      titleFn() {return TAPi18n.__('Continent')},
      width: "5%",
      data: 'continent',
      orderData: [4,6]
    },
    {
      title: '<i class="fa fa-fw fa-wrench"></i>',
      width: "1%",
      className: "f-table-actions",
      tmpl: Meteor.isClient && Template.frontend__results__actions
    },

    {data: 'start', visible: false},
  ],
  selector: function(user_id) {
    // let
    //   start_of_week = moment().tz('Europe/Berlin').startOf('week').toDate(),
    //   end_of_Week   = moment().tz('Europe/Berlin').endOf('week').toDate();

    // console.log("start_of_week, end_of_Week:", start_of_week, end_of_Week);

    // return {start: { $gte: start_of_week, $lt: end_of_Week }};
    return {};
  },
};