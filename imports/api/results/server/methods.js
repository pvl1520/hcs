import Collection from '../collection';

import object_value from '/imports/api/lib/helpers/object_value';
import is_number from '/imports/api/lib/helpers/is_number';
import moment from 'moment-timezone';

//noinspection JSUnusedGlobalSymbols
Meteor.methods({
  /**
   * Remove result
   * @param _id
   * @returns {*}
   */
  method__result_remove (_id) {
    check(_id, String);
    return Collection.remove({_id});
  },
  /**
   * Calculate durations difference in percent
   * @param _id
   * @param [selector]
   * @returns {string|*}
   */
  method__durations_difference ({_id, selector = {}}) {
    check(_id, String);
    check(selector, Match.Maybe(Object));
    // console.log("method__durations_difference - _id:", _id);

    const this_object = Collection.findOne({_id});

    function duration(start, end) {
      if (!start || !end) {
        return false;
      }

      start = moment(start);
      end = moment(end);

      return end.diff(start, 'seconds');
    }

    let this_duration = duration( object_value(this_object, 'start'), object_value(this_object, 'end') );

    if (!this_duration) {
      return;
    }

    selector._id = {$ne: _id};
    // selector.start = {$exists: true};
    // selector.end = {$exists: true};

    // Get all durations
    let durations = Collection.find(selector).map(object => {
      const object_duration = duration( object_value(object, 'start'), object_value(object, 'end') );

      if ( object_duration
        && typeof object_duration !== "undefined"
        && object_duration > 0
      ) {
        return object_duration;
      }
    });
    // console.log("method__durations_difference - durations 0:", durations);

    durations =
      _.chain(durations)
        // .flatten()
        .compact()
        // .uniq()
        .value();
    // console.log("method__durations_difference - durations 1:", durations);
    // console.log("method__durations_difference - durations.length:", durations.length);

    if (durations.length <= 0) {
      return;
    }

    let sum = 0;

    // Sum all real durations
    durations.map(num => {
      if (is_number(num)) {
        sum = sum + num;
      }
    });
    // console.log("method__durations_difference - sum:", sum);

    // Get average difference
    let average_difference = sum / durations.length;
    // console.log("method__durations_difference - average_difference:", average_difference);

    // Calc this duration percent for average one and check if this percent is positive or negative
    let difference_percent = (this_duration * 100 / average_difference) - 100;
    // console.log("method__durations_difference - difference_percent:", difference_percent);

    // Round result
    difference_percent = Math.round(difference_percent);

    return difference_percent;
  },
  /**
   * Get players list
   * param [selector]
   * @returns {[string]}
   */
  method__players_list (selector) {
    check(selector, Match.Maybe(Object));

    let players = Collection.find(selector).map(result => {
      let array = object_value(result, 'players');
      if (array) {
        return array;
      }
    });
    // console.log("method__players_list - players 0:", players);

    players =
      _.chain(players)
        .flatten()
        .compact()
        .uniq()
        // .sort() // Case sensitive
        .sortBy(i => i.toLowerCase())
        .value();
    // console.log("method__players_list - players 1:", players);

    return players;
  }
});