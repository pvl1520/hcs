import './template.html';

import Results from '/imports/api/results/collection';

import is_number from '/imports/api/lib/helpers/is_number';
import moment from 'moment-timezone';

Template.frontend__results.onCreated(function () {
  // console.log("Template.frontend__results.onCreated - this:", this);

  const instance = this;

  instance.select_player = new ReactiveVar();

  instance.players = new ReactiveVar([]);

  instance.autorun(() => {
    method();

    let
      // start_of_week = moment().tz('Europe/Berlin').startOf('week').toDate(),
      // end_of_Week   = moment().tz('Europe/Berlin').endOf('week').toDate(),
      // selector = { start: { $gte: start_of_week, $lt: end_of_Week } };
      selector = {};

    Results.find(selector).observeChanges({
      changed: function (id, fields) {
        // console.log("changed", id, fields)
        method();
      },
      added: function (id, fields) {
        // console.log("added", id, fields)
        method();
      },
      removed: function (id) {
        // console.log("removed", id)
        method();
      }
    });

    function method () {
      Meteor.call('method__players_list', selector, (error, result) => {
        if (error) {
          console.error("Template.frontend__results.onCreated - method__players_list - error:", error);
        } else {
          // console.log("Template.frontend__results.onCreated - method__players_list - result:", result);
          instance.players.set(result);
        }
      });
    }
  });
});

Template.frontend__results.helpers({
  players() {
    const players = Template.instance().players.get();
    console.log("Template.frontend__results.helpers - players:", players);
    return players;
  },
  selector() {
    const player =  Template.instance().select_player.get();

    if (!player) {
      return {};
    }

    return {
      players: {$in: [ player ] }
    };
  }
});

Template.frontend__results.events({
  'click .js-add-result': _.throttle(function (event, instance) {
    Modal.open({
      data: {
        template: 'frontend__create_result',
        // html_attributes: {
        //   'data-modal-content-no-padding': true
        // }
      }
    });
  }, 700),
  'change .js-select-player': _.throttle(function (event, instance) {
    const $this = $(event.target);
    instance.select_player.set( $this.val() );
  }, 700),
  'click #lg_en'(event) {
    TAPi18n.setLanguage('en');
    },
  'click #lg_ru'(event) {
    TAPi18n.setLanguage('ru');
    },
});


Template.frontend__results__duration.onCreated(function () {
  // console.log("Template.frontend__results__duration.onCreated - this:", this);

  this.durations_difference = new ReactiveVar();

  const _id = this.data._id;

  let
    // start_of_week = moment().tz('Europe/Berlin').startOf('week').toDate(),
    // end_of_Week   = moment().tz('Europe/Berlin').endOf('week').toDate(),
    // selector = { start: { $gte: start_of_week, $lt: end_of_Week } };
    selector = {};

  Meteor.call('method__durations_difference', {_id, selector}, (error, result) => {
    if (error) {
      console.error("Template.frontend__results__duration.onCreated - method__durations_difference - error:", error);
    } else {
      // console.log("Template.frontend__results__duration.onCreated - result:", result);
      this.durations_difference.set(result);
    }
  });
});

Template.frontend__results__duration.helpers({
  duration_difference() {
    let
      duration_difference = Template.instance().durations_difference.get(),
      result = {};

    if (!is_number(duration_difference)) {
      return false;
    }

    result.number = Math.abs(duration_difference);

    if (duration_difference > 0) {
      result.direction = 'positive';
    }

    if (duration_difference < 0) {
      result.direction = 'negative';
    }

    return result;
  }
});


Template.frontend__results__actions.events({
  'click .js-remove': _.throttle(function (event, instance) {
    const _id = this._id;

    const r = confirm(TAPi18n.__('Delete') + '?');
    if (r) {
      Meteor.call('method__result_remove', _id, function (error) {
        if (error) {
          console.log("error:", error.message);
          $notify(TAPi18n.__('Error') + '\n' + error.message, 'warning');
        } else {
          $notify(TAPi18n.__('Deleted'), 'success');
        }
      });
    }
  }, 700),
});


Template.frontend__create_result.helpers({
  Results,
  selected_players() {
    let result_autoform = AutoForm.getFieldValue("players", "form__create_result");

    if (!result_autoform) {
      return [];
    }

    return result_autoform.map(value => {
      return {value, label: value};
    });
  },
  winner_disabled() {
    let result_autoform = AutoForm.getFieldValue("players", "form__create_result");

    return !(Array.isArray(result_autoform) && result_autoform.length > 0);
  },
  end_dateTimePickerOptions() {
    let start = moment( AutoForm.getFieldValue("start", "form__create_result") ).toDate();
    console.log("start:", start);

    let options = {
      format: "YYYY-MM-DD HH:mm",
      sideBySide: true,
      // showTodayButton: true,
      showClear: true,
      useCurrent: false
    };

    if (start) {
      // options.minDate = start;
      // options.defaultDate = start;
    }

    return options;
  }
});

AutoForm.addHooks(['form__create_result'], {
  onSuccess: function(formType, result) {
    // console.log('Autoform onSuccess - money_add - formType, result, this:', formType, result, this);
    $notify(TAPi18n.__('Added'), 'success');

    let view = Blaze.getView($('form#form__create_result').closest('.js-modal')[0]);
    // console.log('Autoform onSuccess - money_add - view:', view);
    Modal.close(view);
  },
  onError: function (formType, error) {
    $notify(TAPi18n.__('Error') + '\n' + error, 'error');
  }
});