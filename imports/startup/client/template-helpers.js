import object_value from '/imports/api/lib/helpers/object_value';
import { numeral } from '/imports/api/lib/helpers/numeral';

Template.registerHelper("lib_numeral", function(num) {
  numeral.locale(TAPi18n.getLanguage());
  return numeral(num).format();
});

Template.registerHelper("lib_version", function() {
  let version = object_value(Meteor, 'settings.public.version');
  // console.log("Code version:", version);
  return version;
});