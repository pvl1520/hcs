import { onPageLoad } from "meteor/server-render";
onPageLoad(sink => {
  sink.appendToHead('<meta name="viewport" content="width=device-width" />');
});