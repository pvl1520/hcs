﻿/**
 * Import templates
 */
import '/imports/ui/base';
import '/imports/ui/components/not_found';
import '/imports/ui/components/loading';

import '/imports/api/results/client/template';

/**
 * Set general routes
 */

FlowRouter.route('/', {
  name: 'frontend.front',
  action() {
    BlazeLayout.render('frontend__base', {
      main: 'frontend__results'
    });

    DocHead.setTitle('Results');
  },
});

/**
 * Set routes for 404 page
 * We could not create `route_group.notFound` routes handler for every group.
 * https://github.com/kadirahq/flow-router/issues/308
 */
FlowRouter.notFound = {
  action: function () {
    console.log("Router: notFound");

    // ...for frontend
    BlazeLayout.render('frontend__base', {
      header: 'frontend__header',
      main: 'frontend__not_found',
      footer: 'frontend__footer'
    });

    DocHead.setTitle('Not found');
  }
};